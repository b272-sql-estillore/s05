[1]
SELECT customerName FROM customers 	WHERE country = "Philippines";

[2]
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

[3]
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

[4]
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

[5]
SELECT customerName FROM customers WHERE state IS NULL;

[6]
SELECT firstName, lastName, email FROM employees WHERE lastName="Patterson" AND firstName = "Steve";

[7]
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

[8]
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

[9]
SELECT * FROM productlines WHERE textDescription LIKE '%state of the art%';

[10]
SELECT DISTINCT country FROM customers;

[11]
SELECT DISTINCT status FROM orders;

[12]
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

[13]
SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON offices.officeCode = employees.officeCode AND offices.city = "Tokyo";

[14]
SELECT customers.customerName FROM customers JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie" ;

[15]
SELECT productName, customerName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode WHERE customers.customerName = "Baane Mini Imports";

[16]
SELECT firstName, lastName, customerName, offices.country FROM offices JOIN employees ON employees.officeCode = offices.officeCode JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE offices.country = customers.country;

[17]
SELECT productName, quantityInStock FROM productlines JOIN products ON products.productLine = productlines.productLine WHERE products.productLine = "planes" AND quantityInStock < 1000;

[18]
SELECT customerName FROM customers WHERE phone LIKE "+81%";